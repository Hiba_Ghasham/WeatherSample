
import Foundation
import RealmSwift


//Use protocols to transform our struct to Realm Object

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject() -> ManagedObject
}
