//
//  Follower.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 22/11/2021.
//

import Foundation
// MARK: - Follower
struct Follower: Codable , Equatable{
   
    let id: Int
    let login : String
   
    let avatarURL: String?
    let type: String?
   
  
    
    enum CodingKeys: String, CodingKey {
        case  id
        case login
        case avatarURL = "avatar_url"
      
        case type
       
      
      
    }
}
