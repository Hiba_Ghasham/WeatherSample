import Foundation

// MARK: - GetFailureReason
public struct AppError : Error , Equatable
{
    var reason : GetFailureReason?
    var message : String?
    init(reason :GetFailureReason , message : String? )
    {
        self.reason = reason
        self.message = message
    }
}
enum GetFailureReason: Int, Error
{
    case unAuthorized = 401
    case userNameOrPasswordUnCorrect = 402
    case Forbidden = 403
    case notFound = 404
    case unprocessableEntity = 422
    case notModified = 304
    case networkError = 700
    case urlGeneration = 701
    case unKnownError = 702
}
