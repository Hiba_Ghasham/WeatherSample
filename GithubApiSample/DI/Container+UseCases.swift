//
//  Container+Services.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation
import Swinject
import SwinjectAutoregistration

extension Container {
    func registerUseCases() {

       
        
        
        autoregister(GetUserProfileUseCase.self, initializer: DefaultGetUserProfileUseCase.init)
        
       
        autoregister(GetFollowersUseCase.self, initializer: DefaultFollowersUseCase.init)
        
        autoregister(GetFollowingUseCase.self, initializer: DefaultGetFollowingUseCase.init)
        
      
        
    }
}
