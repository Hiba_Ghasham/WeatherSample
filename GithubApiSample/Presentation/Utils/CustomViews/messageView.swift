
import Foundation
import UIKit
import SwiftMessages

class messageView {
    
    static func showHintMessage(message: String?) {
        
        SwiftMessages.defaultConfig.presentationStyle = .center
        
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Hold on", body: message ?? "You shouldn't reach this page", iconText: "")
        
        view.button?.isHidden = true
        
        // Hide when message view tapped
        view.tapHandler = { _ in SwiftMessages.hide() }
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        
        view.layoutMarginAdditions = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)

        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        view.backgroundView.cornerRadius = 10
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
    
    static func showSuccessMessage(message: String?) {
        
        SwiftMessages.defaultConfig.presentationStyle = .center
        SwiftMessages.defaultConfig.duration = SwiftMessages.Duration.seconds(seconds: 2)
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Great!!", body: message ?? "Everything went well!", iconText: "")
        
        view.button?.isHidden = true
        
        // Hide when message view tapped
        view.tapHandler = { _ in SwiftMessages.hide() }
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)

        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        view.backgroundView.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    
    static func showWarningMessage(message: String?) {
        
        SwiftMessages.defaultConfig.presentationStyle = .center
        SwiftMessages.defaultConfig.duration = SwiftMessages.Duration.seconds(seconds: 2)
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.button?.isHidden = true
        
        // Hide when message view tapped
        view.tapHandler = { _ in SwiftMessages.hide() }
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        
        view.configureContent(title: "Warning", body: message ?? "Please check your connection and try again", iconText: "")
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        view.backgroundView.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    
    
    
    static func showCopyingMessage(message: String?) {
        
        SwiftMessages.defaultConfig.presentationStyle = .center
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.info)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.button?.isHidden = true
        
        // Hide when message view tapped
        view.tapHandler = { _ in SwiftMessages.hide() }
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        
        view.configureContent(title: "", body: message ?? "Operation completeted successfuly", iconText: "")
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        view.backgroundView.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    
}

