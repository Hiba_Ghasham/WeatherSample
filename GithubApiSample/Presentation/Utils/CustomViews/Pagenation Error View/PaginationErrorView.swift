//
//  PaginationErrorView.swift
//  twofour54-ios
//
//  Created by Rama Shaaban on 30/05/2021.
//

import Foundation
import UIKit

protocol PaginationErrorViewDelegate  {
    func didTapRetryPaging()
}

class PaginationErrorView: UIView, CardViewDelegate {
        
    var delegate: PaginationErrorViewDelegate?
    
    @IBOutlet weak var cardView: CardView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardView.delegate = self
     
    }
    
    func didTap(cardView: CardView) {
        self.delegate?.didTapRetryPaging()
    }
    
    
}
