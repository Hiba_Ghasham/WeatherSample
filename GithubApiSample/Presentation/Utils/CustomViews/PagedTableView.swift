//
//  PagedTableView.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 22/11/2021.
//
import Foundation
import UIKit

open class PagedTableView: UITableView {
    private var loadingView: UIView!
    private var indicator: UIActivityIndicatorView!
    
    public func CellRegister(nibName : String , Identifier : String)
    {
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: Identifier)
    }
    
    public func showFooterLoading() {
        if loadingView == nil {
            createLoadingView()
        }
        self.tableFooterView = loadingView
    }
    
    public func hideFooterLoading() {
        self.reloadData()
        
        self.tableFooterView = nil
    }
    
    private func createLoadingView() {
        loadingView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 50))
        indicator = UIActivityIndicatorView()
        indicator.color = UIColor.lightGray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.startAnimating()
        loadingView.addSubview(indicator)
        centerIndicator()
        self.tableFooterView = loadingView
    }
    
   
    private func centerIndicator() {
        let xCenterConstraint = NSLayoutConstraint(
            item: loadingView, attribute: .centerX, relatedBy: .equal,
            toItem: indicator, attribute: .centerX, multiplier: 1, constant: 0
        )
        loadingView.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(
            item: loadingView, attribute: .centerY, relatedBy: .equal,
            toItem: indicator, attribute: .centerY, multiplier: 1, constant: 0
        )
        loadingView.addConstraint(yCenterConstraint)
    }
}
