//
//  ShadowView.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 22/11/2021.
//

import Foundation
import UIKit
class ShadowView : UIView {
    var viewLabel: String! {
        didSet {
            
        }
    }
    
    required init(coder aDecoder: NSCoder!)  {
        super.init(coder: aDecoder)!
        self.setShadowWithColorAndCorner(color: UIColor.black, opacity: 0.2 , offset: CGSize(width: 0, height: 0), radius: 4, viewCornerRadius: 20)
        
    }
}
