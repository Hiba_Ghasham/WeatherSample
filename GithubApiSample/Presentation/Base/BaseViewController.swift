
import UIKit
let errorViewTag = 2020
class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func showFullScreenLoading()
    {
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        
    }
    
    func finishFullScreenLoading()
    {
        // DispatchQueue.main.async {
        
        LoadingOverlay.shared.hideOverlayView()
        // }
        
        
    }
    
    public func showFooterError(delegate: PaginationErrorViewDelegate, tableView: UITableView) {
        
        let allviews = Bundle.main.loadNibNamed("PaginationErrorView", owner: self, options: nil)
        
        if let footerView = allviews?.first as? PaginationErrorView {
            
            footerView.delegate = delegate
            
            tableView.tableFooterView = footerView
            
        }
    }
    
    public func showFullScreenError(view: UIView, message: String = "" , title : String = "" , delegate : PaginationErrorViewDelegate) {
        
        let allviews = Bundle.main.loadNibNamed("PaginationErrorView", owner: self, options: nil)
        
        if let errorView = allviews?.first as? PaginationErrorView {

            errorView.delegate = delegate
         
            errorView.tag = errorViewTag
          
          
            errorView.center =  CGPoint(x: self.view.frame.width  / 2,
                                        y: ( self.view.frame.height)  / 2)
            
            view.addSubview(errorView)
            
        }
    }
    
    public func hideFullScreenError() {
        
        if let errorView = self.view.viewWithTag(errorViewTag) {
            errorView.removeFromSuperview()
        }
    }
    
}
