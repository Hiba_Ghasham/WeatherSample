//
//  FavouriteViewController.swift
//  BaseProject
//
//  Created by Mohammed Jarkas on 21/09/2021.
//

import UIKit
import RxSwift
 
class FollowViewController: BaseViewController  {
    
   
    @IBOutlet weak var tableView: PagedTableView!
    
    @IBOutlet weak var backView: CardView!
    var deletedIndex : Int = -1
    var isPagingNow : Bool = true
    var viewModel: FollowersViewModel!
    let disposeBag = DisposeBag()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.configureTableView()
        
        self.tableView.CellRegister(nibName:  "FollowersTableViewCell", Identifier: "FollowersTableViewCell")
        
        
        self.loadPage()
        backView.delegate = self
    }
    
    
    func loadPage()
    {
       
        let observable = viewModel.load()
        observable.subscribe(onNext: {
            result in
            switch result
            {
            case .loading:
                if self.viewModel.nextPage == 1
                {
                    self.showFullScreenLoading()
                }
                else{
                    self.tableView.showFooterLoading()
                }
                
                break
            case .failure(_):
               
                if self.viewModel.nextPage == 1
                {
                    self.finishFullScreenLoading()
                    DispatchQueue.main.async {
                        self.showFullScreenError(view: self.view, delegate: self)
                    }
                }
                else{
                    self.tableView.hideFooterLoading()
                    self.showFooterError(delegate: self, tableView: self.tableView)
                    
                }
                break
            case .success(_, _ , _):
                self.finishFullScreenLoading()
                self.tableView.reloadData()
                self.tableView.hideFooterLoading()
                break
            }
        }).disposed(by: disposeBag)
    }
    
    
    
    
    private func updateItems() {
        tableView.reloadData()
    }
    

    func configureTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    
}
extension FollowViewController: UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell",
                                                       for: indexPath) as? FollowersTableViewCell else {
            
            return UITableViewCell()
        }
        cell.configure(user: viewModel.items[indexPath.row])
        return cell
    }
 
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height )
        {
            
            if  viewModel.shouldLoadPage()
            {
                loadPage()
            }
        }
    }
    
}


// MARK: - Instance
extension FollowViewController {
    
    static func getInstance() -> FollowViewController {
        
        let followersViewController = FollowViewController(nibName: "FollowViewController", bundle: nil)
        
        return followersViewController
    }
}

extension FollowViewController: PaginationErrorViewDelegate , CustomErrorViewDelegate
{
    
    
    func didTapRetryPaging()
    {
        self.hideFullScreenError()
        tableView.tableFooterView = nil
        loadPage()
    }
    
    func didTapRetry()
    {
        self.hideFullScreenError()
        loadPage()
        
    }
}

extension FollowViewController : CardViewDelegate
{
    func didTap(cardView: CardView) {
   
        MainCoordinator.shared?.pop()
    }
    
    
}
