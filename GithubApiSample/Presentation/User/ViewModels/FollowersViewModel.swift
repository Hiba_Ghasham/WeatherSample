//
//  FollowersViewModel.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 22/11/2021.
//

import Foundation
import RxSwift
enum FollowerType  {
    case followers
    case following
    
}

final class FollowersViewModel {
    
    private let getFollowersUseCase: GetFollowersUseCase
    private let getFollowingUseCase : GetFollowingUseCase
    // INITIAL STATE - CURRENT PAGE EQUAL ZERO
    // DONT LOAD ANY PAGE YET
    var currentPage: Int = 0
    
    var hasMorePages: Bool = true
    var nextPage: Int { hasMorePages ? currentPage + 1 : currentPage }
    var loading : Bool = false
    
    var currentResult : Result<[Follower]>? = nil
    
    var userName : String = ""
    
    var items: [Follower] = [Follower]()
    
    var followType : FollowerType = .followers
    // MARK: - Init
    
    init(getFollowersUseCase: GetFollowersUseCase , getFollowingUseCase : GetFollowingUseCase)
    {
        self.getFollowersUseCase = getFollowersUseCase
        self.getFollowingUseCase = getFollowingUseCase
        
    }
    
    // MARK: - Private
    
    private func appendPage(_ followers: [Follower])
    {
        currentPage += 1
        
        hasMorePages =  followers.count < 10 ? false : true
        
        
        items += followers
    }
    
    private func resetPages()
    {
        currentPage = 0
      
        items.removeAll()
    }
    
    func load() -> Observable<Result<[Follower]>>
    {
        
        if followType == .followers
        {
            return getFollowersUseCase.execute( with: userName, page: nextPage).do(onNext:  { result in
                self.currentResult = result
                switch result {
                case .success( let data , _ , _) :
                    
                    self.appendPage(data)
                    
                    self.loading = false
                    break
                case .failure(_):
                    self.loading = false
                    break
                case .loading:
                    self.loading = true
                }
            })
        }
        else
        {
            return getFollowingUseCase.execute( with: userName, page: nextPage).do(onNext:  { result in
                self.currentResult = result
                switch result {
                case .success( let data , _ , _) :
                    
                    self.appendPage(data)
                    
                    self.loading = false
                    break
                case .failure(_):
                    self.loading = false
                    break
                case .loading:
                    self.loading = true
                }
                
            })
        }
    }
    
    private func update() {
        resetPages()
        
    }
    
    func shouldLoadPage() -> Bool
    {
        if hasMorePages
        {
            switch currentResult
            {
            case .success(_, _, _):
                return true
            default:
                return false
            }
            
        }
        return false
    }
}
