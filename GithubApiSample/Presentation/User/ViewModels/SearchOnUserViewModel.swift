//
//  SearchOnUserViewModel.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation
import RxSwift

final class SearchOnUserViewModel
{

    let searchBarPlaceholder = NSLocalizedString("Search User", comment: "")
    
    var user : User?
    private let getUserProfileUseCase: GetUserProfileUseCase
  
    // MARK: - Init

    init(getUserProfileUseCase: GetUserProfileUseCase)
    {
        self.getUserProfileUseCase = getUserProfileUseCase
    }

    // MARK: - SEARCH ON USER BY NAME
   
    func search(with name : String) -> Observable<Result<User>>
    {
        return getUserProfileUseCase.execute(with: name).do(onNext:  { result in
            
            switch result {
            case .success( let data , _ , _) :
                self.user = data
                break
            case .failure(_):
                
                break
            case .loading:
                break
            }
            
        })
    }

}
