//
//  GetUserProfileUseTests.swift
//  GithubApiSampleTests
//
//  Created by Mohammed Jarkas on 22/11/2021.
//

import XCTest
import RealmSwift
@testable import GithubApiSample

class DefaultUserRepositoryTests: XCTestCase {
    
    
   
    
   // let success_result : Result<GithubApiSample.User> = .success(data: DefaultUserRepositoryTests.user, code: 200, message: "success")
    
    let error_result : Result<GithubApiSample.User> = .failure(AppError(reason: .notFound, message: "User not found"))
    
    var repositoryMock : RepositoryMock!
    var mockedStorage : MockedStorage!
    
    
    
    override func setUpWithError() throws {
        repositoryMock = RepositoryMock()
        mockedStorage = MockedStorage()
       
    }
    
    override func tearDownWithError() throws {
        repositoryMock = nil
        mockedStorage = nil
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func testFetchUser_whenSuccessfullyFetches_thenUserIsSavedInStorage() {
        //given
        let query = "r"
        
        //  when
        repositoryMock.fetchUser(with: query , cached: {_ in
            
        }, completion: { result in
            // then
            let user =  self.mockedStorage.loadUser(query)?.toUser()
            
            XCTAssertEqual(user?.login , "r")
            
        })
    }
    
    func testFetchUser_whenUserNotFound_thenResultIsFailure()
    {
        //given
        let query = "-qjsjsjj-" // invalid username according to github standard
        //  when
        repositoryMock.fetchUser(with: query , cached: {_ in
            
        }, completion: { result in
            // then
            
            XCTAssertEqual(result, .failure(AppError(reason: .notFound, message: "User not found")))
        })
    }
    
    
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    enum RepositorySuccessTestError: Error {
        case failedFetching
    }
    
    class RepositoryMock : UserRepository
    {
        
        let mockStorage = MockedStorage()
        func fetchUser(with name: String, cached: @escaping (UserObject) -> Void, completion: @escaping (Result<GithubApiSample.User>) -> Void) {
            
            
            if name == "r"
            {
                let user = GithubApiSample.User(id: 50374, login: "r", name: "Raffi Krikorian", company: "", avatarURL: "https://avatars.githubusercontent.com/u/50374?v=4", type: "user", location: "San Francisco, CA", bio: "", followers: 88, following: 0)
                mockStorage.saveUser(user.managedObject())
                completion(.success(data: user, code: 200, message: "success"))
                
            }
            else{
                completion(.failure(AppError(reason: .notFound, message: "User not found")))
            }
        }
        
        func fetchFollowersList(with name: String, page: Int, completion: @escaping (Result<[Follower]>) -> Void) {
            
        }
        func fetchFollowingList(with name: String, page: Int, completion: @escaping (Result<[Follower]>) -> Void) {
            
        }
        
    }
    
    class MockedStorage : UserStorage
    {
        func saveUser(_ user: UserObject) {
            
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "MyInMemoryRealm"))
            
            try! realm.write
            {
                realm.add(user)
            }
        }
        
        func loadUser(_ name: String) -> UserObject? {
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "MyInMemoryRealm"))
            let predicate = NSPredicate(format: "login = %@ OR login = %@", name.lowercased() , name.uppercased())
            return realm.objects(UserObject.self).filter(predicate).last
            
        }
        
        
    }
    
}
